<?php
namespace App\Controllers\Dev;
use App\Core\Controller as Controller;
/**
 *  Paths
 */
class Phpinfo extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function IndexAction()
	{
			$this->view->render('d0', 'phpinfo');
	}

    protected function before(){}

    protected function after(){}

} //END CLASS