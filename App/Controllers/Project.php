<?php
namespace App\Controllers;
use App\Core\Controller as Controller;
/**
 *  Project
 */
class Project extends Controller
{

	public function __construct()
	{
		parent::__construct();

		//Setting global css file
		self::setStyles('app');
	}

	public function indexAction()
	{
			$this->view->render('f1', 'project');
	}

    protected function before(){}

    protected function after(){}


} //END CLASS