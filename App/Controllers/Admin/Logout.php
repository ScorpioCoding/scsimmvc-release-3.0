<?php
namespace App\Controllers\Admin;
use App\Core\Controller as Controller;
/**
 *  Logout
 */
class Logout extends Controller
{
    public function __construct()
	{
		parent::__construct();
	}

    public function IndexAction()
	{
			$this->view->render('b0', 'logout');
	}

    protected function before(){}

    protected function after(){}


} //END CLASS