<?php
namespace App\Controllers\Admin;
use App\Core\Controller as Controller;
/**
 *  Project
 */
class Project extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function indexAction()
	{
			$this->view->render('b1', 'project');
	}
    protected function before(){}

    protected function after(){}

} //END CLASS