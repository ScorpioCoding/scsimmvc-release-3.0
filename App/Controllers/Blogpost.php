<?php
namespace App\Controllers;
use App\Core\Controller as Controller;
/**
 *  Blogpost
 */
class Blogpost extends Controller
{

	public function __construct()
	{
		parent::__construct();

		//Setting global css file
		self::setStyles('app');
	}

	public function indexAction( $id = array(), $title = array() )
	{
		$scVariables = self::getScVariables();
		$scVariables['scTitle'] = $title;

		//input
		$input = array( 'id' => $id);

		self::setStyles('sc');
		$scStyles = self::getStyles();

		$data = array_merge($scVariables, $scStyles, $input);

		$this->view->render('f1', 'blogPost' , $data);
	}

	protected function before(){}

    protected function after(){}

}
