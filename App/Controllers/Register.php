<?php
namespace App\Controllers;
use App\Core\Controller as Controller;
/**
 *  Register
 */
class Register extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function indexAction()
	{
			$this->view->render('b0', 'register');
	}

	protected function before(){}

    protected function after(){}

} //END CLASS