<?php
namespace App\Controllers;
use App\Core\Controller as Controller;
/**
 *  Home
 */
class Home extends Controller
{

	public function __construct()
	{
		parent::__construct();

		//Setting global css file
		self::setStyles('app');
	}

	public function indexAction()
	{
		$this->view->render('f1', 'home');
	}
	
    protected function before(){}

    protected function after(){}

} //END CLASS