<?php
namespace App\Core;
/**
*  Base View
*/
class View extends Functions
{
	public function __construct()
    {
		parent::__construct();
	}

	/*
	* render the view
	* @params int 		$renderOption  (f0, f1, b0, b1) 0: noInclude, f: frontend, b: backend,
	* @params string 	$name	
	* @params array 	$data
	*/
	public function render($renderOption = 1, $name, $data = array() )
	{


// define("PATH_VIEW", PATH_APP . "Views" . DS);
// define("PATH_VIEW_TMP", PATH_VIEW. "Templates" . DS);
// define("PATH_VIEW_ADMIN", PATH_VIEW . "Admin" . DS);
// define("PATH_VIEW_DEV", PATH_VIEW. "Dev" . DS);


		$paths = array (
			'f0' => 
				array ( 1 => PATH_VIEW . $name . '.phtml'),
			'f1' =>
				array ( 1 => PATH_VIEW_TMP . 'header.phtml',
						2 => PATH_VIEW . $name . '.phtml',
						3 => PATH_VIEW_TMP . 'footer.phtml'),
			'b0' => 
				array ( 1 =>  PATH_VIEW_ADMIN. $name . '.phtml'),
			'b1' =>
				array ( 1 => PATH_VIEW_TMP . 'header.phtml',
						2 => PATH_VIEW_ADMIN . $name . '.phtml' ,
						3 => PATH_VIEW_TMP . 'footer.phtml'),
			
			'd0' => 
				array ( 1 =>  PATH_VIEW_DEV. $name . '.phtml'),
			'd1' =>
				array ( 1 => PATH_VIEW_TMP . 'header.phtml',
						2 => PATH_VIEW_DEV . $name . '.phtml' ,
						3 => PATH_VIEW_TMP . 'footer.phtml'),
		);

		
		self::renderPage($renderOption, $paths, $data );			
	}

} //END CLASS
?>

