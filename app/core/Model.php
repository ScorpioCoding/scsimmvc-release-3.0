<?php
namespace App\Core;
/**
 * Base Model
 */
class Model extends Functions
{
        public function __construct() 
        {
            parent::__construct();
            $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        }
} //END CLASS
?>
